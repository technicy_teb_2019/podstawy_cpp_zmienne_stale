#include <iostream>
#include <cmath>


using namespace std;

int main() {
    srand(time(NULL));



    /*
     * Do tej pory używaliśmy zmiennych tzw. lokalnych. Zmienne lokalne
     * tworzone są "lokalnie" czyli w pamięci (RAM), która rezerwowana
     * jest przez system operacyjny dla aktualnie wykonywanej funkcji.
     * W naszym wypadku jest to funkcja main. Wszystkie zmienne
     * tworzone w ramach funkcji nazywa się często zmiennymi
     * stosowymi (zmienne, tak jak i funkcja układane są sekwencyjnie
     * w pamięci do wykonania, co często przypomina stos - to, znalazło się
     * na górze jest pierwsze wykorzystywane.
     *
     * LIFO - Last In First out (stos)
     * FIFO - First In First Out
     * FILO - First In Last Out
     * LILO - Last In Last Out
     *
     *
     * Niestety pamięć lokalna funkcji ma pewne ograniczenia - przeważnie
     * jest w stanie JEDNOCZEŚNIE pomieścić dane, które mogą zmieścić się
     * od ok. 2 kB do 256 kB (potrzebna weryfikacja).
     *
     * Innym problemem jest izolacja tych wartości właśnie w obrębie
     * tylko jednej funkcji - aby skorzystać z tak zapisanych danych
     * musimy je np. na nowo tworzyć w innych funkcjach.
     *
     * ISTOTNE - zmienne lokalne/stosowe zachowują WARTOŚC, którą
     * chcemy przechować i ją domyślnie pobierają. Oznacza to,
     * że nie wiemy do końca w której komórce/części pamięci zostaje
     * zapisana wartość.
     *
     * Rozwiązaniem powyższego problemu są zmienne wskaźnikowe.
     * W przeciwieństwie do zmiennych lokalnych zapisywane są na tzw.
     * stercie - pamięci zarezerwowanej i zarządzanej najczęściej
     * przez system operacyjny, która nie posiada ograniczeń pamięci
     * lokalnej (chociażmy wielkościowej). Ponadto zmienne wskaźnikowe
     * NIE ZAPIUJĄ WARTOŚCI a miejsce (ADRES KOMÓRKI PAMIĘCI), pod którą
     * znajduje się wartość.
     * Znajdąc umiejscowienie wartości w pamięci możemy bez przeszkód
     * odwoływać się do takiego miejsca z dowolnego miejsca programu
     * (niekoniecznie z jednej funkcji/bloku instrukcji). Oznacza to,
     * że tak zapisywane wartości możemy bez przeszkód wykorzystywać
     * w dowolnej części programu.
     *
     * Wadą zmiennych wskaźnikowych jest fakt, że nawet po zakończeniu
     * programu będą one przechowywały zapisaną w pamięci wartość (np.
     * bardzo tajne hasło albo kod autoryzacyjny banku). Dlatego zmienne
     * te programista musi kasować sam na koniec ich używania.
     *
     * Zmienne wskaźnikowe możemy wykorzystywać jako pojedyncze wartości
     * albo jako tzw. zmienne tablicowe (zmienne zbiorcze), gdzie pod jedną
     * nazwą możemy zapisać N wartości; N - określona przez nas maksymalna
     * ilość zmiennych.
     */
    //zmienna wskaźnikowa w C++ ZAWSZE zaczyna się
    // tzw. asteriksem (gwiazdką) przed nazwą zmiennej
    //zmiennej tej NIE TRZEBA inicjalizować wartością domyślną
    //za to poniższa dekracja
    int *w;
    //nie tworzy tej zmiennej - po prostu określamy, że chcemy
    //z takiej zmiennej skorzystać. W tej postaci ta zmienna
    //NIE ISTNIEJE (wyrażamy chęć jej późniejszego powołania
    //"do życia").
    //powołanie do życia:
    w = new int;
    //wskazujemy, że zmienna w ma dostać nową lokalizacje w pamięci
    //(stąd operator new) oraz powierdzamy, że zmienna jest typu
    //int (nie chodzi tyle o typ co ilość zajętych komórek
    //pamięci od adresu, w którym będziemy przechowywać wartość)
    cout << "Podaj losowanie:\n1. Lotto\n2. Multilotek\n3. Szczęśliwy numerek";
    cout << "\nWybór: ";
    //zmienna nie wyświetli zawartości - wyświetli numer
    //komórki pamięci, w której zaalokowano wartość
    //cout << w <<endl;
    //tak możemy wyświetlić zawartość pamięci
    //żeby się do niej odwołać MUSIMY napisać przed
    //zmienną asteriksa
    //cout << *w << endl;
    //aby zapisać wartość pod zmienną wskaźnikową musimy
    //przed nazwą zmiennej postawić asteriksa
    cin >> *w;

    cout << "Podana wartość to: " << *w << endl;

    //zmienne wskaźnikowe, jak już zostało wspomniane,\
    //pozwalają także na tworzenie zmiennych grupujących
    //(tablicowych - arrays). Zmienne tego typu mogą
    //przechowywać więcej niż pojedynczą wartość.
    //Utworzenie takiej zmiennej - deklaracja standardowa
    int *liczby;
    //przypisanie pamięci - tutaj prócz typu podajemy także
    //w nawiasach kwadratowych ile wartości pod tą zmienną
    //będziemy chcieli przechowywać:
    liczby = new int[6];
    //powyższa zmienna będzie zawierała 6 zmiennych typu int
    //odwołanie do takich zmiennych:
    liczby[0] = 12;
    liczby[1] = -5;
    liczby[2] = 1567;
    liczby[3] = 90;
    liczby[4] = -133;
    liczby[5] = 15;
    //zmienne tablicowe posiadają indeksy liczone od zera
    //do wartości N-1 (maksymalnej ilości pomniejszonej o 1
    //przez wzgląd na numerowanie od zera)
    //powyżej przypisano wartości wszystkich sześciu zmiennym
    //przy okazji pokazując jak się do nich odwoływać!

    int liczba1,liczba2,liczba3,liczba4,liczba5,liczba6;

    liczba1=rand()%49+1;
    liczba2=rand()%49+1;
    liczba3=rand()%49+1;
    liczba4=rand()%49+1;
    liczba5=rand()%49+1;
    liczba6=rand()%49+1;

    cout << "Wylosowano liczby: "
         << "\nLiczba 1 " << liczba1
         << "\nLiczba 2 " << liczba2
         << "\nLiczba 3 " << liczba3
         << "\nLiczba 4 " << liczba4
         << "\nLiczba 5 " << liczba5
         << "\nLiczba 6 " << liczba6 << endl;

    //koedy już nie potrzebujemy zmiennej wskaźnikowej/kończymy
    //program MUSIMY ją sami skasować!
    delete w;
    //kasowanie zmiennych tablicowych jest nieco inne:
    delete [] liczby;
    //musimy pamiętać o nawiasach kwadratowych pomiędzy delete a
    //nazwą zmiennej
    return 0;
}
