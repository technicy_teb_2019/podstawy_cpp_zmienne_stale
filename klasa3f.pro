TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        funkcje.cpp \
        main.cpp \
        operatory.cpp \
        petle.cpp \
        podstawy.cpp \
        typy_danych.cpp
