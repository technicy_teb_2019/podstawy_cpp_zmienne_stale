///*
// * Poniżej przykład załączania do naszego programu biblioteki z zewnętrznym kodem.
// * Kod taki może być udostępniony przez:
// * - kompilator (własnem innowacyjne rozwiązania)
// * - system (standardowe operacje - We/Wyj, matematyczne, zdarzeniowe itp.)
// * - innych programistów (ulepszanie funkcjonalności, dodawanie nowej funkcjonalności)
// * - siebie samych (możemy dołączać wcześniej napisany kod by krócej i sprawniej pisać nowe aplikacje)
// *
// * Biblioteki posiadają przeważnie albo dostęp do elementów już skomipilowanych i używanych
// * w systemie (biblioteki dll), albo do bibliotek, które wkompilujemy w nasz program, lub które
// * dołączymy z naszym programem. Najważniejszą istotą takiego dołączania jest możliwości wykorzystania
// * już gotowych komponentów, których nie musimy sami tworzyć.
// */
//#include <iostream>
////kolejna, przydatna biblioteka (można ich podłączać ile się zechce)
//#include <cmath>

///*
// * Domyślna przestrzeń nazw dla zmiennych i funkcji (a nawet klas). Pozwala na użytkowanie określonych
// * zmiennych bez ciągłego wskazywania na przestrzeń, w której się znajdują. Podczas programowania
// * możemy używać różnych przestrzeni nazw, możemy nawet zmieniać domyślnie używaną poprzez
// * ponowne wywołanie przestrzeni namespace std lub innej.
// */
//using namespace std;

///**
// * @brief Funkcja jest głównym element naszej aplikacji. Bez tej funkcji niemożliwe byłoby uruchowamie
// * czegokolwiek w systemie operacyjnym. Funkcja zawsze musi mieć nazwę main. Za przed nią musi być podany
// * typ int.
// * @return dowolna liczba oznaczająca działanie programu
// */
//int main()
//{
//    int cout =12;//deklaracja i inicjalizacja zmniennej o nazwie cout, której nadajemy wartość 12
//    //linia poniżej jest odpowiedzialna za wyświetlenie na konsoli tekstu oraz wcześniej
//    //zadeklarowanej zmiennej.
//    std::cout << "Hello World!" << " " << cout <<  endl;
//    //ostatnia linia ma za zadanie zwracać dowolną wartość liczbową do systemu.
//    //Wartość 0 to przyjęta jako standrard przez systemy informacja, że program zakończył się poprawnie.
//    //Dowolona inna niż 0 (dodatnia, ujemna) może świadczay o infromacjach, błędach bądź kompunikatach,
//    //które do użytkownika i systemu przesły programita.
//    return 0;
//}
