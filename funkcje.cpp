//#include <iostream>
//#include <cmath>


//using namespace std;

///* Poniżej znajduje się przykład własnej funkcji w języku programowania
// * Funkcje to nic innego jak fragment kodu programu,
// * któremu nadajemy określoną nazwę, dzięki czemu
// * możemy go wywołwywać wielokrotnie w ramach działania
// * naszego programu. Funkcje możemy pisać sami lub
// * pozyskiwać np. z zewnętrznych bibliotek.
// *
// * Każda funkcja musi składac się z następujących elementów:
// *
// * <typ_zwracany> <nazwa_wlasna> (<argumenty>) {
// *         //kod, który chcemy wykonać
// * }
// *
// * typ_zwracany - funkcje mogą zwracać jakieś wartości,
// *                które funkcja ustala podczas swojego
// *                działania (wykonania). Zwracane wartości
// *                mogą być podstawowe (int, double, float, itp.)
// *                lub złożone (je poznamy później) lub
// *                void (z angielskiego pustka - teoretycznie
// *                nic nie zwracane)
// * nazwa_wlasna - dowolna nazwa naszej funkcji, po której
// *                możemy się do niej odwołać. Działa dokładnie
// *                tak samo jak w przypadku nazw zmiennych.
// *                Restrykcje nazewnictwa takie same jak przy
// *                zmiennych.
// * argumenty - funkcje mogą pozyskiwać startowe dane poprzez
// *             poodanie wartości na ich start właśnie w
// *             miejscu okrągłych nawiasów. Funkcje mogą
// *             przyjmować dowolną ilość parametrów, w tym
// *             także parametry-literały czy parametry z
// *             domyślnymi wartościami
// *
// * np. bool parzysta(int wartosc=0) {
// *      if (wartosc%2==0)
// *      //rozkaz return kończy działanie funkcji.
// *      //Ewentualne instrukcje po tym słowie nie będą
// *      //wykonane. W związku z tym, że return true
// *      //pojawi się w spełnionym warunku modulo 2
// *      //to zwróci wartość true (bo parzysta)
// *          return true;
// *      //ta instrukcja return wykona się jeżeli
// *      //poprzedni warunek nie został sepłnony
// *      //więc kod return true nigdy się nie wykonał
// *      return false;
// * }
// *
// * powyższa funkcja zwróci na koniec swojej działalności
// * wartości true (prawda) jeżeli podana liczba pod
// * zmienną wartosc będzie parzysta. Jeżeli wartości nie
// * nie będzie parzysta to funkcja zwróci false;
// * Przykład wywołania:
// * parzysta(23);//wartość false
// * parzysta(14);//wartość true
// * bool wynik = parzysta(15/2);//wartość false, bo wyjdzie
// *                      //7,5  z czego 0,5 zostanie pominięte
// *                      //zostanie 7 czyli nieparzysta
// *
// *
// */
//void suma() {
//    int a,b;
//    a=b=0; //zerowanie wartości - dla pewności
//    cout << "Podaj wartość a: ";
//    cin >> a;
//    cout << "Podaj wartość b: ";
//    cin >> b;
//    cout << "Wynik to: " << a+b << endl;
//}

//void roznica(int a, int b) {
//    cout << "Wynik odejmowania " << a-b << endl;
//}

//int mnozenie(int a, int b) {
//    //cout << "Wypisanie wartości z funkcji: " << a*b << endl;
//    int c = a*b;
//    return c;
//}

//double dzielenie(double a, double b=1) {
//    if(b!=0) {
//        return a/b;
//    }
//    return 0;
//}

////Funkcje ZAWSZE muszą być zainicjalizowane przed
////funkcją main by mogły zostać użytej w tej
////funkcji (znaczy w main). Definicje funkcji zaś mogą
////być wykonywane po ciele main (po jej zakończeniu w kodzie
//// - na końcu plku main.cpp)
////poniżej deklaracja funkcji:
//double potega(double ,double );
///*
// *Powyższa funkcja wskazuje, że my (programiści) możemy
// *w naszym programie, w funkcji main, użyć
// *funkcji o takiej nazwie. Chwilowo nie ma ona
// *sprecyzowanego kodu, jaki ma zostać wykonany.
// *Stąd też nawias  na parametry wjeściowe funkcji
// *zawiera jedynie typy przyjmowanych parametrów,
// *jednak bez ich nazw.
// *
// *Nazwy parametrów oraz ich użycie w kodzie zostaną
// *dodane przy definicji funkcji (poniżej main)
// */

//int main()
//{
//    int jeden=0,dwa=0;

//////    suma();
//////    roznica(10,4);
//////    suma();
////    cout << "Podaj pierwszą liczbę: ";
////    cin >> jeden;
////    cout << "Podaj drugą liczbę: ";
////    cin >> dwa;
////    roznica(jeden/2, dwa);
//////    suma();

////    int zwrocona = mnozenie(2,3);
////    cout << "Zwrócony wynik z funkcji: " << zwrocona << endl;
////    cout << "Wynik 4*5: " << mnozenie(4,5) << endl;
////    cout << "Dzielenie: " << dzielenie(14,2) << endl;
////    cout << "Dzielenie: " << dzielenie(14,0) << endl;
////    cout << "Dzielenie: " << dzielenie(14) << endl;

//    cout << "Potęga: " << potega(3,3) << endl;
//    cout << "Potęga: " << potega(2,5) <<endl;
//    cout << "Potęga: " << potega(0,14) <<endl;
//    return 0;
//}

///*
// * W tym miejscu następuje definicja naszej funkcji
// * o nazwie potega. Dzięki takiemu wybiegowi
// * możemy uczynić nasz kod czytelniejszym
// * szybciej wyświetlając definicję funkcji main.
// * Jak można zauważyć dopiero w tym miejscu pojawiają
// * się nazwy parametrów wejściowych funkcji - a oraz b
// * (nazwy mogą być dowolne, inne).
// */
////double potega(double a, double b) {
////    double c = 1;
////    for (int i=0;i<b;i++) {
////        c*=a; //c=c*a;
////    }
////    return c;
////}

///*
// * Funkcje mogą też być budowane jako tzw.
// * rekurencyjne. Funkcja rekurencyjna to taka, która
// * potrafi wywoływać siebie samą. Funkcja taka
// * działa w zasadzie jako zapętlona (wykonuje kod,
// * w którym woła samą siebie wy konuje na nowo swój
// * kod, aż spotka na nowo swoje wywołanie i rozpoczyna
// * działanie od nowa... w nieskończoność).
// * Dlatego ważne jest by takiej funkcji nadać
// * moment zakończenia działania, tak by wszystkie
// * wywołane wcześniej funkcje także mogły się zakończyć.
// *
// * Rekurencji używamy w przypadku, gdy nie jesteśmy
// * w stanie określić ilość pętli wymaganych
// * do wykonanania zadania lub do skrócenia
// * kodu pętli wykonania.
// *
// * Jako przykład zmienimy poprzednią funkcję potega
// * na rekurencyjną.
// */
//double potega(double a, double b) {
//    if (b==0)
//        return 1;
//    return a*potega(a,b-1);
////    double c = 1;
////    for (int i=0;i<b;i++) {
////        c*=a; //c=c*a;
////    }
////    return c;
//}
////np. potega(2,3)
////I krok
////b nie jest 0 więc if zostanie pomięty
////jako zwrot działania funkcji
////będzie działanie 2*potega(2,3-1)
////II krok
////ponieważ przy zwrocie wywołano funkcję potega
////ale ze zmienionym parametrem b (teraz ma wartosc 2)
////znowu sprawdzimy wartość b (dalej większa od 0)
////więc znowu zwracamy wartosc
//// 2*potega(2,2-1)
////III krok
////b nie jest 0 więc if zostanie pomięty
////jako zwrot działania funkcji
//// będzie działanie 2*potega(2,1-1)
////IV krok
////b równe jest 0 więc zwracamy 1
////WRACAMY DO KROKU III
//// czyli 2
////WRACAMY DO KROKU II
//// czyli 2*2
//// ZWRACAMY DO KROKU 1
////czyli 2*4 (będzie to dokładnie 8)
////zwrócony wynik przez rekurencję to 8
