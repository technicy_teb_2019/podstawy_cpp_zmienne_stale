//#include <iostream>
//#include <cmath>


//using namespace std;




//int main()
//{

//    /*
//     * Operatory w programowau pozwalają  na dokonywanie operacji zarówno bitowych,
//     * liczbowych, tekstowych oraz kontroli przypiswania wartości do zmiennych.
//     * Poniżej znajduje się opis poszczególnych operatorów wraz z przykładem zastosowań
//     */
//    //+ - operator dodawania liczb, tekstu lub wartości bitowych
////    int a = 5+6;
////    string str = "Hello";
////    string str2 = " world";
////    string str3 = str+str2;
////    str3 = string("inny tekst") + string(" i kolejny");
////    cout << str3 << a;

//    //- - operator odejmowania
////    int b = 67-15;
////    double c =  78.4-1984.235667;
//    //* - operator mnożenia, operator wskaźnikowy
////    int d = 67*2;
////    double *e = new double;
////    *e = 12.5;
////    cout << "Komórka pamięci " << e << " zawiera wartość " << *e;
////    delete e;

//    // / - operator dzielenia
////    double f = 56.f / .5f;

//    //= - operator przypisawnia wartości
////    int g = 56;
////    char h = ' ';
////    h = h + ' ';
////    cout << h;
//    //% - modulo liczby (wartość reszty z dzielenia)
////    int ab = 56%33;
////    int ac = 5%2;
////    cout << "Wynik: " << ac << endl;


//    //OPERATORY LOGICZNE
//    //< - operator mniejszości (czy wartość po lewej stronie jest mniejsza od prawej)
////    bool i = 56 < 189;
////    cout << i;

//    //> - opertor większośc (czy wartość po lewej stronie jest większa od prawej)
////    bool j = 56 > 189;
////    cout << i;

//    //<= - operator mniejszy bądź równy
////    bool k = 56 < 56;
////    bool l = 56 <= 56;
////    cout << "Pierwsza: " << k << ", druga: " << l << endl;


//    //>= - operator większy bądź równy
////    bool m = 56 > 56;
////    bool n = 56 >= 56;
////    cout << "Pierwsza: " << m << ", druga: " << n << endl;

//    //== - operator równości
////    bool o = 56==56;
////    bool p = 56==100;
////    cout << "Pierwsza: " << o << ", druga: " << p << endl;
//    //!= - operator nierówności (wartość po lewej stronie inna niż wartość po prawej stronie)
////    bool q = 56!=56;
////    bool r = 56!=100;
////    cout << "Pierwsza: " << q << ", druga: " << r << endl
//    //! - znak logiczny negacji (zaprzeczenia wartości). UWAGA - operator ten
//    //    oznacza również silnię (factorial), aczkolwiek nie działła w przpacku c++
////      bool s = true;
////      cout << "Pierwsza: " << s << ", druga: " << !s << endl;
//    //&& - operator logiczny AND (mnożenie logiczne)
////    bool t = 56 <111 && 7!=12;
////    bool u = 11<=10 && 7!=12;
////    cout << "Pierwsza: " << t << ", druga: " << u << endl;
//    //|| - operator logiczny OR (suma logiczna)
////    bool v = 56<111 || 7==12;
////    bool w = 11<=10 || 7==12;
////    cout << "Pierwsza: " << v << ", druga: " << w << endl;

//    //OPERATORY BITOWE
//    //& - mnożenie bitowe (logiczne)
//    // 5 -> 101 (bitowo)
//    // 1 -> 001 (bitowo)
//    //mnożymy bitowo bit każdy z każdym
////    int x = 5 & 1;
////    cout << "Wynik: " << x << endl;
//    //| - suma logiczna (logiczne)
//    //  101
//    // 1001
//    // 1101 -> 13
////    int y = 5 | 9;
////    cout << "Wynik: " << y << endl;
//    //^ - różnica logiczna (XOR)
//    // 0101
//    // 1001
//    // 1100
////    int z = 5 ^ 9;
////    cout << "Wynik: " << z << endl;
//    //~ - logiczne odwrócenie wartości liczbowej;
//    // 00000000 00000000 00000000 00001001
//    // 11111111 11111111 11111111 11110110
////    int aa = ~9;
////    cout << "Wynik: " << aa << endl;

//    //<< - operator strumieni, operator przesuwania bitów w lewo
//    //1 -> 1
//    // 100000000
////    int piekneSlowo = 1<<8;
////    cout << "Wynik: " << piekneSlowo << endl;
//    //>> - operator strumieni, operator przesuwania bitów w prawo
//    int af = 1024>>5;
//    cout << "Wynik: " << af << endl;

//    //w pzyszłym tygodniu sprawdzić jak działają operatory skróconych operacji
//    //np. +=, -=, &= itd.
//    //znaleźć i wypróbować każdy z nich
//    return 0;
//}
