//#include <iostream>
//#include <cmath>


//using namespace std;


///* Poniżej znajduje się przykład wykorzystywania stałych w języku programowania.
// * Poniższa deklaracja stałych to element tzw. makro w języku C/C++. C++ ten rodzaj
// * definiowania stałych zapożycza z C. W C++ można jeszcze twoprzyć stałe przy pomocy
// * słowa const przy typie zmiennej (omówienie w innym terminie).
// *
// * Niezaprzeczalnym atutem stałych w językach programowania jest przypisanie do nich
// * konkretnych wartości, które możemy później wykorzystywac w programach wielokrotnie,
// * a przy ewentualnej zmianie tejże wartości automatycznie wszystkie miejsca wystąpienia
// * stałej zostaną zastąpione nowymi wa
// * rtościami. Co ważne, w przeciwieństwie do zmiennych
// * stałe zadeklarowane poprzez makro nie występują po stronie pamięci głównej systemu, a
// * po stronie pamięci kodu programu (przeważnie jest jej więcej).
// * Oczywiście niekiedy bardzo cennym rozwiązaniem jest to, że w żaden sposób raz przypisanej
// * wartości nie da się zmienić w żadnym fragmencie programu (nawet przypadkowo - co często
// * dzieje się ze zmiennymi).
// */

//#define TAB3 "\t\t\t|"
//#define TAB2 "\t\t|"
//#define TAB "\t|"

//int main()
//{
//    //nazwy, możliwości, zastosowanie w przyszłym tygodniu
//    //nazwy zmiennych mogą być dowolnymi nazwami wymyślonymi przez programistę. Ważne jest by
//    //nie były one nazwami zastrzeżonymi w języku C++ (np. int, char, main, namespace itp.)
//    //Ponadto zmienne nie mogą rozpoczynać się od cyfry (aczkolwiek mogą zawierać cyfry w nazwie).
//    //Zmienne nie mogą, poza _ (podłogą), nie mogą zawierać znaków specjalnych (!@#$%^&*() itd.)
//    //Nazwy zmiennych powinny (najlepiej) odwoływać się do ich przeznaczenia - znacznie ułatwia to
//    //czytanie kodu, kiedy siadamy do niego po określonym czasie. Oczywiście to programista decyduje
//    //o nazwach, deklaracji (czyli linijce, w której pojawia się przypisanie do zmiennej typu) czy
//    //definicji/inicjalizacji (przypisanie pierwszej wartości) zmiennej

//    //WAŻNE! Zmienne w C++ ZAWSZE powinny być deklarowane z inicjalizacją wartości. Nie zrobienie tego
//    //może skutkować przypisaniem śmieciowej wartości (takiej, jaka aktualnie znajdowałaby się w pamięci
//    //przydzielonej dla zmiennej).

//    //Zmienne zawsze przechowują określony typ wartości (szczególnie tyczy się to języków kompilowanych).
//    //W przypadku próby przypisania wartości niezgodnej z typem zmiennej kompilator może zareagować na kilka
//    //sposobów - niejawnie (bez wiedzy programisty zmienić typ na właściwy; możliwa utrata pierwotnej wartości)
//    //przekonwertować jeden typ w drugi, jawie obciąć wartość po przecinku (zapis zmiennoprzecinkowy do typu całkowitego),
//    //zapisać wartość ze znakiem (ujemną) jako bardzo dużą bez znaku (ostatni bit ma największą wartość!),
//    //zgłosić błąd alokacji pamięci (jeżeli typ będzie np. mniejszy niż wartość do niego zapisywana).

//    //Zmienne pozwalają np. na przechwytywanie wartości od użytkownika, zapis wartości przeliczonych z innych
//    //wartości i zmiennych, przemieszczanie wartości pomiędzy aplikacjami (w ramach aplikacji działających
//    //na tym samym komputerze).

//    //Pojęcie literału - to STAŁA wartość zapisana przez programistę w dowolnym momencie (kodzie) aplikacji.
//    //Literał zapisany do zmiennej przestaje literałem, gdyż w kolejnych liniach kodu jeżeli będziemy
//    //odwoływać się do tej wartości przez nazwę zmiennej, która mogła zostać w międzyczasie zmodyfikowana.

//    bool zmiana= false; //przyjmuje tylko dwie waertości - 0 i 1 (fałsz/prawda); 1 bajt (8 bit)
//    char znak = 'c'; //przechowuje 1 znak z tablicy ASCII; 1 bajt (8bit)
//    //wszystkie pozostałe zmienne (poniżej)  w C++ nie mają stałej wielkości/
//    //ich wielkość (ilość zajętych bajtów) bezpośrednio zależy od architektury porcesora i/lub
//    //systemu operacyjnego
//    short krotka_liczba = -16; //krótka liczba całkowita (ze znakiem); 2 bajty (16 bit) ale nie mniej niż char
//    int liczba = 10; //liczba całkowita (ze znakiem); 4 bajty (32 bity) ale mnie mniej niż short
//    long dliczba = -113; //długa liczba całkowita (ze znakiem); synonim int ale może być 8 bajtowa (64 bit)
//    long long bdliczba = 1455353; //bardzo długa liczba całkowita (ze znakiem); 8 bajtów (64 bity) nie mniej niż long

//    //wartości zmiennoprzecinkowe (bo przecinek może się przemieszczać). Liczby te pozwalają na zapis wartości
//    //tzw. rzeczywistych (czyli ze znakiem +/- oraz wartości ułamkowych). Wartości ułamkowe zapisywane są w
//    //postaci 7 bit (float - pojedyncza precyzja) bądź 15 bit (podwójna precyzja).
//    float zmienna=.0f; //liczba zmiennoprzecinkowa 4 bajty (32 bit)
//    double podw_zmienna = .0; //liczba zmiennoprzecinkowa podwójnej precyzji 8 bajtów (64 bity)

//    //wartości bezznakowe. Dzięki nie posiadaniu wartości ujemny (zawsze dodatnie) możemy zapisać
//    //dwukrtonie większe wartości liczbowe naturalne.
//    unsigned short ukrotka_liczba = 16;
//    unsigned int uliczba = 10;
//    unsigned long udliczba = 113;
//    unsigned long long ubdliczba = 1455353;

//    //W C++ (a także każdym innym języku programowania, za wyjątkiem języków eterycznych)
//    //ciągi znakowe, ponieważ bazują na ASCII, umożliwiają na wykorzystanie dodatkowych,
//    //specjalnych znaków (teoretycznie niedrukowalnych). Co istotne
//    //każdy znak specjalny MUSI zostać poprzedzony znakiem backslash.
//    //Do najwazniejszych znaków specjalnych należą:
//    //\n - znak nowej linii (można, a wręcz trzeba stosować zamiast endl)
//    //\r - znak powrotu kursora tekstu do początku TEJ SAMEJ linii
//    //\t - tabulacja (zwielokrotniona spacja, wielkość zależna od systemu)
//    //\0 - znak null (zero bezwzględne w komputrze); pojawienie się tego znaku powoduje zakończenie
//    //     aktualnego ciągu znakowego.
//    //\\ - wyświetlenie backslash (domyślnie nie jest to możliwe)
//    //\" - wyświetlenie cudzysłowia (domyślnie cudzysłów jest początkiem/ograniczeniem ciągu znakowego)
//    //\' - drukowanie apostrofu jako znaku.


//    cout << "Typ"<< TAB3 << "Przykładowa wartość\t|\tWielkosc (bajty)\n";
//    cout << "-----------------------------------------------------------------------\n";
//    cout << "bool" << TAB3 << zmiana << "\t\t\t|" << sizeof(zmiana)<< "\n";
//    cout << "char" << TAB3 << znak << "\t\t\t|" << sizeof(znak)<< "\n";
//    cout << "short" << TAB3 << krotka_liczba << "\t\t\t|" << sizeof(krotka_liczba)<< "\n";
//    cout << "int" << TAB3 << liczba << "\t\t\t|" << sizeof(liczba)<< "\n";
//    cout << "long" << TAB3 << dliczba << "\t\t\t|" << sizeof(dliczba)<< "\n";
//    cout << "long long\t\t|" << bdliczba << "\t\t|" << sizeof(bdliczba)<< "\n";
//    cout << "float\t\t\t|" << zmienna << "\t\t\t|" << sizeof(zmienna)<< "\n";
//    cout << "double\t\t\t|" << podw_zmienna << "\t\t\t|" << sizeof(podw_zmienna)<< "\n";

//    cout << "unsigned short\t\t|" << ukrotka_liczba << "\t\t\t|" << sizeof(ukrotka_liczba)<< "\n";
//    cout << "unsigned int\t\t|" << uliczba << "\t\t\t|" << sizeof(uliczba)<< "\n";
//    cout << "unsigned long\t\t|" << udliczba << "\t\t\t|" << sizeof(udliczba)<< "\n";
//    cout << "unsigned long long\t|" << ubdliczba << "\t\t|" << sizeof(ubdliczba)<< "\n";

//    return 0;
//}
